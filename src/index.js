/**
 * Created by LeutrimNeziri on 30/03/2019.
 */
import ReactDOM from 'react-dom'
import React from 'react'
import App from 'App'
import { Router, Route, Link, Switch } from 'react-router-dom'
import store, { history } from 'Store'

/** Some Examples */
// match.params // { projectId: 324252, dashboardId: 2354252, datasourceId: 2353262}
// agon-2345-23565-235326
// <Route path="/topics/:projectId/:dashboardId/:datasourceId" component={Topics} />

// const Topic = (props) => {
//     return <h3>Requested Param: {props.match.params.id} {props.match.params.dataSourceId}</h3>
// }
// const Topics = (props) => {
//     const { match } = props
//     console.log('match', match)
//     return (
//         <div>
//             <h2>Topics</h2>

//             <Route path={`${match.path}/:id/:dataSourceId`} component={Topic} />
//             <Route
//                 exact
//                 path={match.path}
//                 render={() => <h3>Please select a topic.</h3>}
//             />
//         </div>
//     )
// }
// const Home = (props) => <div>Welcome home</div>
// const About = (props) => <div>Welcome to about</div>

// const AppTonin = (props) => {
//     return (
//     <Router history={history}>
//         <div>
//             <h4>Welcome</h4>

            // <Route exact path="/" />
//             <Route path="/about" component={About} />
//             <Route path="/topics" component={Topics} />
//         </div>
//     </Router>
//     )
// }

ReactDOM.render(<App />, document.getElementById('root'))


