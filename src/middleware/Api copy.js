import 'whatwg-fetch'
import { API_URL } from 'Constants'

const callApi = ({ endpoint, options = {} }, store) => {
    const url = API_URL + endpoint
    console.log(`callig API: ${url}`)
    return fetch(url, options)
        .then((response) => {
            if (response.status >= 200 && response.status < 300) {
                return response
            }
            const error = {
                status: response.status,
                message: response.statusText
            }
            console.log(`Error at ${url}`, error)
            throw error
        }).then(response => {
            return response.json()
        }).then(json => {
            console.log(`Success at ${url}`, json)
            return json
        })
}


export const CALL_IMAGE_API = 'Call Image API' // Symbol('Call Image API')

/**
 * Intercepts CALL_IMAGE_API actions to perform the call to the API server
 */
export default store => next => action => {
    const call = action[CALL_IMAGE_API]
    // Only apply this middleware if we are calling an API
    if (typeof call === 'undefined') {
        return next(action)
    }
    return callApi(call, store)
}